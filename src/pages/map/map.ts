import { Component, ViewChild, ElementRef } from '@angular/core';

import { Platform } from 'ionic-angular';
import { CommandControllerProvider } from '../../providers/command-controller/command-controller';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {

  singleValue = 0;

  connected:boolean = false;
  arduinoName:string = "Not Connected";
  currentSpeed:number = 4;
  distance:number = 25;

  constructor( public platform: Platform, private controller:CommandControllerProvider) {

  }

  ionViewDidLoad() {
        //subscribe to status here!
   /* this.controller.getStatus()
    .subscribe(data => {
      this.arduinoName = data.name;
      this.currentSpeed = data.speed;
      this.distance = data.distance;
      this.connected = true;
    },
    (err) => {
      console.log("ERROOOOR2");
      this.arduinoName = "Not Connected";
      this.currentSpeed = 0;
      this.distance = 0;
      this.connected = false;
    });*/
  }

  getArduinoName(){
    //return "greenBot";
   // return this.arduinoName;
   return this.arduinoName;
  }

  

  getCurrentSpeed(){
    //return 4;
    //return this.controller.getCurrentSpeed();
    return this.currentSpeed;
  }

  getDistanceToClosestObject(){
    //return 25;
    //return this.controller.getDistanceToClosestObject();
    return this.distance;
  }

  commandForward(){
    this.controller.moveForward();
  }

  commandBackward(){
    this.controller.moveBackward();
  }

  commandLeft(){
    this.controller.moveLeft();
  }

  commandRight(){
    this.controller.moveRight();
  }

  commandStop(){
    this.controller.stopMoving();
    this.singleValue = 0;
  }
  
  sendSpeed(){
    this.controller.setSpeed(this.singleValue);
  }

}
