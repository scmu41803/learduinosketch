import { Component, ViewChild } from '@angular/core';

import { AlertController, App, FabContainer, ItemSliding, List, ModalController, NavController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { CommandControllerProvider } from '../../providers/command-controller/command-controller';
/*
  To learn how to use third party libs in an
  Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
*/
// import moment from 'moment';

import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {

  excludeTracks: any = [];


  bluetoothSettings: string = "";
  bluetoothIsEnabled:boolean = false;
  bluetoothIsConnected:boolean = false;

  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private bluetoothSerial: BluetoothSerial,
    private controller:CommandControllerProvider

  ) {
    
  }

  wifiConnection(){
    return true;
    //return this.controller.isConnected();
  }
  
  connectBluetooth(){
    this.bluetoothSerial.list();
    this.bluetoothSerial.connect('20:17:03:13:07:72').subscribe( (data) => {
    this.bluetoothIsConnected = true;
   },
   () => {
     this.bluetoothIsConnected = false;
   });
  }

  ionViewDidLoad() {
    this.app.setTitle('Smart Car');
  }

  presentFilter() {
    let modal = this.modalCtrl.create(ScheduleFilterPage, this.excludeTracks);
    modal.present();

    modal.onWillDismiss((data: any[]) => {
      if (data) {
        this.excludeTracks = data;
      }
    });

  }


}
