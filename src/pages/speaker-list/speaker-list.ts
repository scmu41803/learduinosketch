import { Component } from '@angular/core';

import { ActionSheet, ActionSheetController, Config, NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ConferenceData } from '../../providers/conference-data';

import { CommandControllerProvider } from '../../providers/command-controller/command-controller';


@Component({
  selector: 'page-speaker-list',
  templateUrl: 'speaker-list.html'
})
export class SpeakerListPage {
  actionSheet: ActionSheet;
  speakers: any[] = [];
  autonomousDrive:boolean;
  singleValue = 0;

  avoidObstacles:boolean = false;
  followLine:boolean = false;

  connected:boolean = false;
  arduinoName:string = "Not Connected";
  currentSpeed:number = 4;
  distance:number = 25;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public config: Config,
    public inAppBrowser: InAppBrowser,
    private controller: CommandControllerProvider
  ) { 

  }

  ionViewDidLoad() {
            //subscribe to status here!
   /* this.controller.getStatus()
    .subscribe(data => {
      this.arduinoName = data.name;
      this.currentSpeed = data.speed;
      this.distance = data.distance;
      this.connected = true;
    },
    (err) => {
      console.log("ERROOOOR2");
      this.arduinoName = "Not Connected";
      this.currentSpeed = 0;
      this.distance = 0;
      this.connected = false;
    });*/
  }

  getArduinoName(){
    

    return this.arduinoName;
  }

  getCurrentSpeed(){
    return this.currentSpeed;

  }

  getDistanceToClosestObject(){
    return this.distance;

  }

  commandStop(){
    this.controller.stopMoving();
    this.singleValue = 0;
  }

  toggleAutonomousDrive(){
    if(!this.autonomousDrive){
      this.controller.stopMoving();
      this.avoidObstacles = false;
      this.followLine = false;
    }
  }

  startAutonomousDrive(){
    this.controller.startAutonomousDrive();
  }

  startLineFollow(){
    this.controller.startLineFollow();
  }

  sendSpeed(){
    this.controller.setSpeed(this.singleValue);
  }

}
