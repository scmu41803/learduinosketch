import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/repeat';
import 'rxjs/add/operator/delay';
import {Observable} from 'rxjs/Rx';

/*
  Generated class for the CommandControllerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CommandControllerProvider {

  private arduinoName:string = "Not Connected";
  private wifiConnection:boolean = false;

  constructor(public http: Http) {
  }

  /*
  getStatus(){
    return this.http.get("http://192.168.0.12:3000/status").map(res => res.json());
  } */

  getStatus(){
    let d = {
      "name":"Not Connected",
      "distance":25,
      "speed":24
    };
    /*
    return Observable
    .timer(0, 3000)
    .concatMap( () => this.http.get("http://192.168.0.12:3000/status"))
    .timeout(5000)
    .retryWhen(error => error)
    .catch( err => {
      return Observable.throw(d);
    } )
    .map(res => res.json()); */

    return Observable
    .timer(0, 1500)
    .concatMap( () => (this.http.get("http://10.22.107.126:3000/status")))
    .timeout(3000)
    .retryWhen(error => {return Observable.throw(error)})
    .map(res => res.json());

  }

  
      intercept(observable: Observable<Response>): Observable<Response> {
          return observable.catch((err, source) => {
              if (err.status  == 401) {
                  return Observable.empty();
              } else {
                  return Observable.throw(err);
              }
          });
    
      }


  moveForward(){
    this.sendCommand('forward');
  }

  moveBackward(){
    this.sendCommand('backward');
  }

  moveLeft(){
    this.sendCommand('left');
  }

  moveRight(){
    this.sendCommand('right');
  }

  stopMoving(){
    this.sendCommand('stop');
  }

  startAutonomousDrive(){
    this.sendCommand('autoDrive');
  }

  startLineFollow(){
    this.sendCommand('lineFollow');
  }

  setSpeed(speed: number){
    this.sendCommand('speed/'+speed);
  }


  private sendCommand( cmd:string ){

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log(cmd);
    this.http.post("http://10.22.107.126:3000/command/"+ cmd,"h").subscribe(res => {
	      	console.log(res.json());
	      }, (err) => {
	      	console.log(err);
	      });;
  }

}
