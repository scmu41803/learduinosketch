
const express = require('express')
const app = express()
var five = require("johnny-five"); 
var board, motors;
var speed = 255;
var sleep = require('sleep');

app.get('/status', function( req, res ){
	res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({
     "name": "greenBot",
     "speed": 4,
 	 "distance":25}));
})



app.post('/command/:cmd', function (req, res) {
	var command = req.params.cmd;
	console.log(command);
	var split = command.split("/");
	switch( split[0] ){
		case "forward":
			forward();
		break;
		case "backward":
			backward();
		break;
		case "right":
			right();
		break;
		case "left":
			left();
		break;
		case "stop":
			stop();
		break;
		case "speed":
			setSpeed(split[1]);
		break;
		case "autonomousDrive":
			autonomousDrive();
		break;
		case "lineFollow":
			lineFollow();
		break;
		default:
			stop();
		break;

	}
	res.status(200).json({status:200});
})



app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
}).on("listening", function () {
	board = new five.Board({
		port:"COM6"
	});

	board.on("ready", function() {

	motors = new five.Motors([
    	{
    		pins: { pwm: 11},
			register: { data: 8, clock: 4, latch: 12 },
			bits: { a: 2, b: 3 }
    	},
    	{
    		pins: { pwm: 3 },
			register: { data: 8, clock: 4, latch: 12 },
			bits: {a: 1, b: 4}
    	},
    	{
    		pins: { pwm: 6 },
			register: { data: 8, clock: 4, latch: 12 },
			bits: {a: 5, b: 7}
    	},
    	{
    		pins: { pwm: 5 },
			register: { data: 8, clock: 4, latch: 12 },
			bits: {a: 0, b: 6}
    	}
  	]);

	var frontLeft = new five.Motor({
		pins: { pwm: 11},
		register: { data: 8, clock: 4, latch: 12 },
		bits: { a: 2, b: 3 }
	});

	var backLeft = new five.Motor({
		pins: { pwm: 3 },
		register: { data: 8, clock: 4, latch: 12 },
		bits: {a: 1, b: 4}
	});

	var frontRight = new five.Motor({
		pins: { pwm: 6 },
		register: { data: 8, clock: 4, latch: 12 },
		bits: {a: 5, b: 7}
	});

	var backRight = new five.Motor({
		pins: { pwm: 5 },
		register: { data: 8, clock: 4, latch: 12 },
		bits: {a: 0, b: 6}
	});


	/*motors.forward(255);

	// frontLeft.forward(255);
	// backLeft.forward(255);
	// frontRight.forward(255);
	// backRight.forward(255);

	board.wait(5000, function() {
		// frontLeft.stop();
		// backLeft.stop();
		// frontRight.stop();
		// backRight.stop();
		motors.stop();
	});

	var proximity = new five.Proximity({
	  controller: "HCSR04",
	  pin: 7
	});

	proximity.on("data", function() {
		console.log("cm: ", this.cm);
	});*/

});
});

// motors[0] = frontLeft, motors[1] = backLeft, motors[2] = frontRight, motors[3] = backRight
function forward (){

	motors.forward(speed);
}

function backward (){
	motors.reverse(speed);
}

function left (){
	// right forward
	// left reverse
	stop();
	motors[2].forward(speed);
	motors[3].forward(speed);
	motors[0].reverse(speed);
	motors[1].reverse(speed);
}

function right () {
	// right reverse
	// left forward
	stop();
	motors[2].reverse(speed);
	motors[3].reverse(speed);
	motors[0].forward(speed);
	motors[1].forward(speed);
}

function stop (){
	motors.stop();
}

function setSpeed(value){
	let a = (value *255 )/100;
	speed = a;
	console.log('speed '+a);
}

function autonomousDrive(){
	blindDrive()
}

function lineFollow(){
	blindDrive();
}

function blindDrive(){
	// 1 = left, 2 = front, 3 = right, 4 = back 
	while(true){
		
		let a = Math.random() * (4 - 1) + 1;
		switch(a){
			case 1: left();
					sleep.sleep(1);
			break;
			case 2: forward();
					sleep.sleep(2);
			break;
			case 3: right();
					sleep.sleep(1);
			break;
			case 4: backward();
					sleep.sleep(2);
			break;			
		}

	}
}


